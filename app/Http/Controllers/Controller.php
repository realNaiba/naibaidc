<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function nbJsTip($msg,$redirect = '/'){
        die("<script>alert('".$msg."');window.location.href='".$redirect."'</script>");
    }

    function nbtip($msg,$status='danger',$link = ['title'=>'返回首页','url'=>'/']){
        echo view('common.info',['status'=>$status,'msg'=>$msg,'link'=>$link]);
        exit();
    }
}
