<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Domain;
use Illuminate\Http\Request;
use App\Lib\FreenomApi;

class DomainController extends Controller
{
    function search(Request $request)
    {
        $this->validate($request, [
            'domainname' => 'required|min:3|max:40',
        ]);

        $api = new FreenomApi();

        return $api->domain_search($request->domainname);
    }

    function whois(Request $request){
        return Domain::where('name',$request->domainname)->first();
    }
}
