<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Lib\Dnspod;
use App\Lib\FreenomApi;
use App\Models\Domain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DomainController extends Controller
{
    function index(Request $request)
    {
        return view('dashboard.domain', ['domains' => $request->user()->domains()->get()]);
    }

    /** 删除域名 */
    function delete(Request $request, $domainname)
    {
        $domain = $request->user()->domains()->where('name', $domainname)->get();
        if (!count($domain) == 1) {
            $this->nbJsTip('这个域名是你的吗？');
        }
        $domain = $domain[0];
        $api = new FreenomApi();
        $result = $api->domain_delete($domainname);
        if (isset($result['domain']) && $result['domain']['status'] == 'DELETED') {
            $dns = new Dnspod();
            $result = $dns->doDomainRemove($domain->dnspod_id);
            Log::info($request->user()->id.'删除了域名'.json_encode($result));
            $domain->dnsrecords()->delete();
            $domain->delete();
            $this->nbJsTip('域名已成功注销', route('dashboard.domain'));
        } else {
            $this->nbJsTip('域名删除失败了');
        }
    }

    /** 注册域名 */
    function reg($domainname)
    {
        //验证域名是否合法
        $domain_avaliable_preg = '/^.{3,30}\.(tk|ga|ml|cf|gq)$/';
        if (empty($domainname) | !preg_match($domain_avaliable_preg, $domainname)) {
            die('error domain name');
        }

        $data = [
            'domainname' => $domainname,
            'domaintype' => 'FREE',
            'actiontype' => 'reg',
            'msg' => '请遵守所在国家相关法律，禁止滥用。',
        ];

        return view('dashboard.domain.reg', $data);
    }

    function regHandler(Request $request, $domainname)
    {
        //验证域名是否合法
        $domain_avaliable_preg = '/^.{3,30}\.(tk|ga|ml|cf|gq)$/';
        if (empty($domainname) | !preg_match($domain_avaliable_preg, $domainname)) {
            die('error domain name');
        }
        //检查用户信息
        $this->validate($request, [
            'name' => 'required|min:3|max:8',
            'qq' => 'required|integer|min:10000|max:10000000000',
        ]);
        //注册域名
        $api = new FreenomApi();
        $domain_reg_result = $api->domain_register($domainname, 'TETI752392', null, false);
        Log::info($request->user()->id . '域名注册:' . json_encode($domain_reg_result));
        $domain = self::checkRegResult($domain_reg_result);
        if ($domain) {
            //添加到Dnspod
            $dnspod = new Dnspod();
            $result = $dnspod->doDomainCreate($domain['domainname']);
            Log::info($request->user()->id . '添加到DNSPOD:' . json_encode($result));
            if ($result->status->code == 1) {
                $dnspod_id = $result->domain->id;            //Dnspod 域名ID
            } else {
                $dnspod_id = -1;            //Dnspod 域名ID
            }
            //域名入库
            $name = $domain['domainname'];               //域名
            $expirationdate = $domain['expirationdate'];//过期时间
            $domain = new Domain([
                'dnspod_id' => $dnspod_id,
                'name' => $name,
                'expirationdate' => $expirationdate,

                'contact_name' => $request->name,
                'contact_qq' => $request->qq,
            ]);

            if ($request->user()->domains()->save($domain)) {
                $this->nbtip('域名[' . $name . '-一年]注册成功!', 'success', ['title' => '域名管理中心', 'url' => route('dashboard.domain')]);
            }
        } else {
            $this->nbtip('域名注册失败了。');
        }
    }

    /** 判断域名是否注册成功 */
    private static function checkRegResult($domain_reg_result)
    {
        if (isset($domain_reg_result['status']) && $domain_reg_result['status'] == 'OK'
            && isset($domain_reg_result['result']) && $domain_reg_result['result'] == 'DOMAIN REGISTERED'
            && isset($domain_reg_result['domain'][0]['domainname'])
            && isset($domain_reg_result['domain'][0]['expirationdate'])
        ) {

            return [
                'domainname' => strtolower($domain_reg_result['domain'][0]['domainname']),
                'expirationdate' => $domain_reg_result['domain'][0]['expirationdate'],
            ];
        }

        return false;
    }
}
