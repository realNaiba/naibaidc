<?php
/**
 * NB(http://blog.niuti.org)
 * ====Copy © 2017====
 */

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Lib\Dnspod;
use Illuminate\Http\Request;

class DnsController extends Controller
{
    private $type = ['A', 'CNAME', 'MX', 'TXT', 'NS', 'AAAA', 'SRV', '显性URL', '隐性URL'];
    private $line = ['默认' => '0', '国内' => '7=0', '国外' => '3=0', '电信' => '10=0', '联通' => '10=1', '教育网' => '10=2', '移动' => '10=3', '百度' => '90=0', '谷歌' => '90=1', '搜搜' => '90=4', '有道' => '90=2', '必应' => '90=3', '搜狗' => '90=5', '奇虎' => '90=6', '搜索引擎' => '80=0'];
    /** 删除解析记录 */
    function delete(Request $request,$domainname){

        $domain = $this->checkPermission($request->user(),$domainname);
        $api = new Dnspod();
        $result = $api->doRecordRemove($domain->dnspod_id,$request->record_id);

        if (property_exists($result,'status') && $result->status->code==1){
            $record = $domain->dnsrecords()->where('dnspod_id',$request->record_id)->first();
            $record->delete();
        }

        $this->nbJsTip($result->status->message,'/dashboard/domain/'.$domainname.'/dns');
    }
    /** 添加解析 */
    function add(Request $request,$domainname){
        if (!isset($this->line[$request->dnsline]))die('error line');
        $domain = $this->checkPermission($request->user(),$domainname);

        $api = new Dnspod();
        $result = $api->doRecordCreate($domain->dnspod_id,$request->subdomain,$request->dnstype,$this->line[$request->dnsline],$request->dnsvalue,$request->mx,$request->ttl);

        if (property_exists($result,'record')){
            $domain->dnsrecords()->create([
                'dnspod_id'=>$result->record->id,
                'sub_domain'=>$request->subdomain==''?'@':$request->subdomain,
                'record_type'=>$request->dnstype,
                'record_line'=>$request->dnsline,
                'value'=>$request->dnsvalue,
                'mx'=>$request->mx,
                'ttl'=>$request->ttl,
            ]);
        }

        $this->nbJsTip($result->status->message,'/dashboard/domain/'.$domainname.'/dns');
    }

    function index(Request $request, $domainname)
    {
        $domain = $this->checkPermission($request->user(), $domainname);
        return view('dashboard.domain.dns', [
            'domain_name' => $domain->name,
            'dnsrecords' => $domain->dnsrecords()->get(),
            'dnstype'=>$this->type,
            'dnsline'=>$this->line,
        ]);
    }

    private function checkPermission($user, $domainname)
    {
        $domain = $user->domains()->where('name', $domainname)->get();
        if (count($domain) != 1) {
            $this->nbtip('这个域名不属于您哦');
        }
        return $domain[0];
    }
}
