<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Hosting;
use HansAdema\MofhClient\Client;
use HansAdema\MofhClient\Exception\ApiException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HostController extends Controller
{
    var $api;

    /**
     * HostController constructor.
     */
    public function __construct()
    {
        $this->api = new Client(env('BYET_NAME'),env('BYET_PASSWORD'));
    }

    function index(Request $request){
        return view('dashboard.hosting',['hostings'=>$request->user()->hostings()->get()]);
    }

    /** 添加主机 */
    function add(Request $request){

        $this->validate($request,[
            'cp_username'=>'required|max:8|alpha_dash|unique:hostings',
            'cp_password'=>'required',
            'domain'=>'required|alpha_dash|unique:hostings|not_in:www,wap,demo',
        ]);

        try{
            $this->api->createAccount($request->cp_username,$request->cp_password,$request->user()->email,$request->domain.'.naiba.ga','naibafree1');
            $hosting = new Hosting([
                'cp_username' => $request->cp_username,
                'cp_password' => $request->cp_password,
                'domain'=>$request->domain,
            ]);
            if($request->user()->hostings()->save($hosting)){
                $this->nbJsTip('主机开通成功，记得右下角加群','/dashboard/hosting');
            }else{
                throw new ApiException('数据库出错',null);
            }
        }catch (ApiException $e){
            $this->nbJsTip('出现错误:'.$e->getMessage(),'/dashboard/hosting/add');
        }

    }
}
