<?php
/**
 * NB(http://blog.niuti.org)
 * ====Copy © 2017====
 */

namespace App\Lib;


class Dnspod
{
    const API = 'https://dnsapi.cn/';
    const TIME_OUT = 30;
    private $public_parameter;

    public function __construct()
    {
        $this->public_parameter = [
            'login_token' => env('DNSPOD_TOKEN'),
            'format' => 'json',
            'lang' => 'cn',
            'error_on_empty' => 'no',
        ];
    }

    /** 获取支持线路 */
    function getRecordLine($domain_id)
    {
        $data = [
            'domain_id' => $domain_id,
            'domain_grade' => 'DP_Free',
        ];
        return $this->naibadns('Record.Line',$data,'post');
    }

    /** 获取记录类型 */
    function getRecordType()
    {
        $data = [
            'domain_grade' => 'DP_Free',
        ];
        return $this->naibadns('Record.Type', $data, 'post');
    }

    private function naibadns($url, $data = array(), $method = 'get')
    {
        //拼接url
        $url = self::API . $url;
        //拼接公共参数
        $data = array_merge($this->public_parameter, $data);
        return json_decode($this->http($url, $data, $method));
    }

    private function http($url, $data = array(), $method = 'get')
    {

        $method = strtolower($method);

        $curl = curl_init();

        switch (strtolower($method)) {
            case 'post':
                curl_setopt($curl, CURLOPT_POST, true);
                break;
            case 'put':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //设置请求体，提交数据包
                break;
            case 'delete':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
            case 'get':
                if (count($data) > 0) {
                    //拼接URL
                    $url = stristr($url, '?') ? $url . '&' . http_build_query($data, '', '&') : $url . '?' . http_build_query($data, '', '&');
                }
                $method = 'get';
                break;
        }

        if (!is_null($data) && $method != 'get') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Don't print the result
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::TIME_OUT);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::TIME_OUT);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Naiba DNS Manager/1.0.0 (root@niuti.org)');

        try {
            $return = curl_exec($curl);
            $this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            //$this->errors['curl'] = curl_getinfo($curl);
        } catch (Exception $ex) {
            $this->errors['curl'] = array(
                'no' => curl_errno($curl),
                'error' => curl_error($curl)
            );

            $return = null;
        }

        curl_close($curl);

        return $return;
    }

    /** 删除域名 */
    function doDomainRemove($domain_id){
        $data = [
           'domain_id'=>$domain_id
        ];

        return $this->naibadns('Domain.Remove',$data,'post');
    }

    /** 添加域名 */
    function doDomainCreate($domain)
    {
        $data = [
            'domain' => $domain,
        ];
        return $this->naibadns('Domain.Create', $data, 'post');
    }
    /** 添加解析 */
    function doRecordCreate($domain_id,$sub_domain,$record_type,$record_line_id,$value,$mx,$ttl,$status='enable'){
        $data = [
            'domain_id'=>$domain_id,
            'sub_domain'=>$sub_domain,
            'record_type'=>$record_type,
            'record_line_id'=>$record_line_id,
            'value'=>$value,
            'mx'=>$mx,
            'ttl'=>$ttl,
        ];
        return $this->naibadns('Record.Create',$data,'post');
    }

    /** 删除解析 */
    function doRecordRemove($domain_id,$record_id){
        $data = [
            'domain_id'=>$domain_id,
            'record_id'=>$record_id,
        ];
        return $this->naibadns('Record.Remove',$data,'post');
    }


}