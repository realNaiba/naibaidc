<?php
/**
 * NB(http://blog.niuti.org)
 * ====Copy © 2017====
 */

namespace App\Lib;

class FreenomApi
{
    /**
     * The API base url
     */
    const URL = 'https://api.freenom.com/v2/';
    /**
     * Storage for the object errors
     * @var number
     */
    public $errors = array();
    /**
     * The timeout for the requests
     * @var number
     */
    private $timeout = 30;
    private $email;         //用户名
    private $password;      //密码

    /**
     * Constructor
     *
     * Checks if the cURL extension is loaded.
     */
    public function __construct()
    {
        if (!extension_loaded('curl')) {
            $this->err("PHP required extension - curl - not loaded.", true);
        }
        $this->email = env('FREENOM_EMAIL');
        $this->password = env('FREENOM_PASSWORD');
    }

    ########################
    # Services
    ########################

    /**
     * Error handler
     *
     * @param String $message
     * @param String $fatal
     */
    private function err($message = 'Error encountered', $fatal = false)
    {
        $this->errors[] = $message;

        if ($fatal == true) {
            echo $message;
            exit;
        }
    }

    ########################
    # Domains
    ########################

    /**
     * Ping the service
     */
    public function ping()
    {
        return $this->ask('service/ping');
    }

    /**
     * Private method to send requests
     *
     * @param String $url
     * @param String $data
     * @param String $method
     */
    private function ask($url, $data = array(), $method = 'get')
    {
        // Make the request
        $response = $this->get(self::URL . $url, $data, $method);

        $response = @json_decode($response, true);

        // Add the encountered non-fatal errors to the response
        if (count($this->errors) > 0) {
            $response['errors'] = $this->errors;
        }

        return $response;
    }

    /**
     * Private method to help sending requests
     *
     * @param String $url
     * @param String $data
     * @param String $method
     */
    private function get($url, $data = array(), $method = 'get')
    {
        $method = strtolower($method);

        $curl = curl_init();

        switch (strtolower($method)) {
            case 'post':
                curl_setopt($curl, CURLOPT_POST, true);
                break;
            case 'put':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //设置请求体，提交数据包
                break;
            case 'delete':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
            case 'get':
                if (count($data) > 0) {
                    $url = stristr($url, '?') ? $url . '&' . http_build_query($data, '', '&') : $url . '?' . http_build_query($data, '', '&');
                }
                $method = 'get';
                break;
        }

        if (!is_null($data) && $method != 'get') {
            if (isset($data['nameserver'])) {
                unset($data['nameserver']);
                $str_data = http_build_query($data) . '&nameserver=f1g1ns1.dnspod.net&nameserver=f1g1ns2.dnspod.net';
                //die($str_data);
            } else {
                $str_data = http_build_query($data);
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str_data);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Don't print the result
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Naiba Domain/1.0.0 (root@niuti.org)');


        try {
            $return = curl_exec($curl);
            $this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            //$this->errors['curl'] = curl_getinfo($curl);
        } catch (Exception $ex) {
            $this->errors['curl'] = array(
                'no' => curl_errno($curl),
                'error' => curl_error($curl)
            );

            $return = null;
        }

        curl_close($curl);

        return $return;
    }

    /**
     * Search for available domains
     */
    public function domain_search($domainname)
    {
        return $this->ask('domain/search', array(
            'domainname' => $domainname,
            'domaintype' => 'FREE',

            'email' => $this->email,
            'password' => $this->password
        ));
    }

    /**
     * Register a domain
     *
     * Note:
     * forward_url and nameservers are mutual exclusive. Either the forward_url OR nameservers need to be specified. It is not possible to specify both. In case the nameserver is under the same domain, a glue record must be created for this domain. This can be done by registering Nameserver records.
     */
    public function domain_register($domainname, $owner_id, $forward_url = '', $nameserver = '')
    {
        return $this->ask('domain/register', array(
            'domainname' => $domainname,
            'owner_id' => $owner_id,
            'email' => $this->email,
            'password' => $this->password,

            'period' => '12M',
            'domaintype' => 'FREE',
            'forward_url' => $forward_url,
            'nameserver' => $nameserver,
        ), 'post');
    }

    /** 删除域名 */
    public function domain_delete($domainname)
    {
        return $this->ask('domain/delete', [
            'domainname' => $domainname,
            'email' => $this->email,
            'password' => $this->password,
        ], 'delete');
    }

    /**
     * Renew a domain name registration
     */
    public function domain_renew($domainname, $email, $password, $period = '1Y')
    {
        return $this->ask('domain/renew', array(
            'domainname' => $domainname,
            'email' => $email,
            'password' => $password,

            'period' => $period
        ), 'post');
    }

    /**
     * Get info on the registered domain names
     */
    public function domain_getinfo($domainname, $email, $password)
    {
        return $this->ask('domain/getinfo', array(
            'domainname' => $domainname,
            'email' => $email,
            'password' => $password
        ));
    }

    /**
     * Modify a domain
     *
     * Note:
     * forward_url and nameservers are mutual exclusive. Either the forward_url OR nameservers need to be specified. It is not possible to specify both. In case the nameserver is under the same domain, a glue record must be created for this domain. This can be done by registering Nameserver records.
     */
    public function dommain_modify($domainname, $email, $password, $owner_id = '', $period = '1Y', $forward_url = '', $nameserver = '', $billing_id = '', $tech_id = '', $admin_id = '')
    {
        return $this->ask('domain/modify', array(
            'domainname' => $domainname,
            'email' => $email,
            'password' => $password,

            'owner_id' => $owner_id,
            'email' => $email,
            'password' => $password,
            'period' => $period,
            'forward_url' => $forward_url,
            'nameserver' => $nameserver,
            'billing_id' => $billing_id,
            'tech_id' => $tech_id,
            'admin_id' => $admin_id
        ), 'post');
    }

    ########################
    # Contact
    ########################

    /**
     * Register or modify a nameserver glue record
     */
    public function nameserver_register($domainname, $hostname, $ipaddress, $email, $password)
    {
        return $this->ask('nameserver/register', array(
            'domainname' => $domainname,
            'hostname' => $hostname,
            'ipaddress' => $ipaddress,
            'email' => $email,
            'password' => $password
        ), 'post');
    }

    /**
     * Deleting a nameserver glue record
     */
    public function nameserver_delete($domainname, $hostname, $email, $password)
    {
        return $this->ask('nameserver/delete', array(
            'domainname' => $domainname,
            'hostname' => $hostname,
            'email' => $email,
            'password' => $password
        ), 'post');
    }

    /**
     * Listing nameserver glue records under a domain
     */
    public function nameserver_list($domainname, $email, $password)
    {
        return $this->ask('nameserver/list', array(
            'domainname' => $domainname,

            'email' => $email,
            'password' => $password
        ));
    }

    /**
     * Create or modify contact
     */
    public function contact_register($contact_title, $contact_firstname, $contact_lastname, $contact_address, $contact_city, $contact_zipcode, $contact_statecode, $contact_countrycode, $contact_phone, $contact_email, $contact_organization = '', $contact_fax = '', $contact_id = '')
    {
        return $this->ask('contact/register', array(
            'contact_title' => $contact_title,
            'contact_firstname' => $contact_firstname,
            'contact_lastname' => $contact_lastname,
            'contact_address' => $contact_address,
            'contact_city' => $contact_city,
            'contact_zipcode' => $contact_zipcode,
            'contact_statecode' => $contact_statecode,
            'contact_countrycode' => $contact_countrycode,
            'contact_phone' => $contact_phone,
            'contact_email' => $contact_email,
            'email' => $this->email,
            'password' => $this->password,

            'contact_organization' => $contact_organization,
            'contact_fax' => $contact_fax,
            'contact_id' => $contact_id
        ), 'post');
    }

    ########################
    # Transfers
    ########################

    /**
     * Delete contact
     */
    public function contact_delete($contact_id, $email, $password)
    {
        return $this->ask('contact/delete', array(
            'contact_id' => $contact_id,
            'email' => $email,
            'password' => $password
        ), 'post');
    }

    /**
     * Get info on specific contacts
     */
    public function contact_getinfo($contact_id, $email, $password)
    {
        return $this->ask('contact/getinfo', array(
            'contact_id' => $contact_id,
            'email' => $email,
            'password' => $password
        ));
    }

    /**
     * List contacts under account
     */
    public function contact_list()
    {
        return $this->ask('contact/list', array(
            'email' => $this->email,
            'password' => $this->password
        ));
    }

    /**
     * Get price of a domain transfer
     */
    public function domain_transfer_price($domainname, $authcode, $email, $password)
    {
        return $this->ask('domain/transfer/price', array(
            'domainname' => $domainname,
            'authcode' => $authcode,
            'email' => $email,
            'password' => $password
        ));
    }

    /**
     * Request a domain transfer
     */
    public function domain_transfer_request($domainname, $authcode, $owner_id, $period = '1Y', $email, $password)
    {
        return $this->ask('domain/transfer/request', array(
            'domainname' => $domainname,
            'authcode' => $authcode,
            'owner_id' => $owner_id,
            'period' => $period,
            'email' => $email,
            'password' => $password
        ), 'post');
    }

    ########################
    # Internally used functions
    ########################

    /**
     * Approve a domain transfer
     */
    public function domain_transfer_approve($domainname, $email, $password)
    {
        return $this->ask('domain/transfer/approve', array(
            'domainname' => $domainname,
            'email' => $email,
            'password' => $password
        ), 'post');
    }

    /**
     * Decline a domain transfer
     */
    public function domain_transfer_decline($domainname, $reason, $email, $password)
    {
        return $this->ask('domain/transfer/decline', array(
            'domainname' => $domainname,
            'reason' => $reason,
            'email' => $email,
            'password' => $password
        ), 'post');
    }

    /**
     * List current domain transfers
     */
    public function domain_transfer_list($email, $password)
    {
        return $this->ask('domain/transfer/list', array(
            'email' => $email,
            'password' => $password
        ));
    }
}