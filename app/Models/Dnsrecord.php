<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dnsrecord extends Model
{
    protected $fillable = [
        'dnspod_id',
        'sub_domain',
        'record_type',
        'record_line',
        'record_line_id',
        'value',
        'mx',
        'ttl',
        'status',
    ];
}
