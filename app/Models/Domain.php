<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'dnspod_id',
        'name',
        'expirationdate',
        'contact_name',
        'contact_qq',
    ];

    protected $hidden=[
        'dnspod_id',
        'update_at',
        'user_id',
        'id',
    ];

    function dnsrecords(){
        return $this->hasMany('App\Models\Dnsrecord');
    }
}
