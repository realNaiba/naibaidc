<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hosting extends Model
{
    protected $fillable = [
        'cp_username',
        'cp_password',
        'domain'
    ];
}
