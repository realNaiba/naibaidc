<?php
/**
 * NB(http://blog.niuti.org)
 * ====Copy © 2017====
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function domains(){
        return $this->hasMany('App\Models\Domain');
    }

    function hostings(){
        return $this->hasMany('App\Models\Hosting');
    }
}
