@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">域名DNS管理( <b>{{$domain_name}}</b> )</div>
                    <div class="panel-body">
                        <form name="add" method="post">{{csrf_field()}}
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>主机名</th>
                                        <th>类型</th>
                                        <th>线路</th>
                                        <th>记录值</th>
                                        <th>MX优先级</th>
                                        <th>TTL</th>
                                        <th>启用</th>
                                        <th><span class="glyphicon glyphicon-flag text-danger"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td scope="row"><input name="subdomain" value="www" type="text" size="7"/></td>
                                        <td><select name="dnstype">
                                                @foreach($dnstype as $type)
                                                    <option value="{{$type}}">{{$type}}</option>
                                                @endforeach
                                            </select></td>
                                        <td><select name="dnsline">
                                                @foreach($dnsline as $k=>$v)
                                                    <option value="{{$k}}">{{$k}}</option>
                                                @endforeach
                                            </select></td>
                                        <td><input name="dnsvalue" value="127.0.0.1" type="text" size="7"/></td>
                                        <td><input name="mx" value="1" type="text" size="1"/></td>
                                        <td><input name="ttl" value="600" type="text" size="3"/></td>
                                        <td><input name="status" type="checkbox" checked/></td>
                                        <td>
                                            <button type="submit" class="btn btn-sm btn-primary">添加记录</button>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>主机名</th>
                                    <th>类型</th>
                                    <th>线路</th>
                                    <th>记录值</th>
                                    <th>TTL</th>
                                    <th>状态</th>
                                    <th>管理</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dnsrecords as $record)
                                    <tr>
                                        <th scope="row">{{$record['id']}}</th>
                                        <td>{{$record['sub_domain']}}</td>
                                        <td>{{$record['record_type']}}</td>
                                        <td>{{$record['record_line']}}</td>
                                        <td>{{$record['value']}}</td>
                                        <td>{{$record['ttl']}}</td>
                                        <td><span class="glyphicon glyphicon-ok-sign text-success"></span></td>
                                        <td><form id="deleteForm" method="post" name="delete"><input type="hidden" name="record_id" value="{{$record['dnspod_id']}}"/><input type="hidden" name="_method" value="DELETE">{{csrf_field()}}<span onclick="$('#deleteForm').submit()" class="glyphicon glyphicon-remove text-danger"></span></form></td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection