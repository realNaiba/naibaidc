@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">确认注册域名</div>

                <div class="panel-body">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="page-header">
                            <h3>确认信息 <small>Naiba</small></h3>
                        </div>
                        <p>域名：<b class="text-danger">{{$domainname}}</b></p>
                        <p>价格：{{$domaintype}}</p>
                        <p>年限：一年（距离到期14天时可以进行续费）</p>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>注意!</strong> {{$msg}}
                        </div>
                        <hr>

                        <form action="/dashboard/domain/{{$domainname}}" class="form-horizontal" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">您的姓名</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('qq') ? ' has-error' : '' }}">
                                <label for="qq" class="col-md-4 control-label">所有人QQ</label>
                                <div class="col-md-6">
                                    <input id="qq" type="number" class="form-control" name="qq" value="{{ old('qq') }}" required>

                                    @if ($errors->has('qq'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('qq') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-danger">确认注册</button>
                            </div>
                            <hr>
                            <p class="text-right">
                                &copy;Naiba
                            </p>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
