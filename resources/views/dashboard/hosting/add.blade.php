@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">开通免费主机</div>

                    <div class="panel-body">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="page-header">
                                <h3>确认信息
                                    <small>Naiba</small>
                                </h3>
                            </div>
                            <p>套餐：<b class="text-danger">Free Ⅰ 型</b></p>
                            <p>价格：$0.00</p>
                            <p>年限：一年</p>
                            <div class="alert alert-warning alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>注意!</strong> 严禁放置秒赞、采集等无用应用，被管理人员发现永久封号(包括但不限于邮箱、手机号)。
                            </div>
                            <hr>

                            <form action="/dashboard/hosting/add" class="form-horizontal" role="form"
                                  method="POST">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('cp_username') ? ' has-error' : '' }}">
                                    <label for="cp_username" class="col-md-4 control-label">管理面板账号</label>
                                    <div class="col-md-6">
                                        <input id="cp_username" type="text" class="form-control" name="cp_username"
                                               value="{{ old('cp_username') }}" required autofocus>

                                        @if ($errors->has('cp_username'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('cp_username') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('cp_password') ? ' has-error' : '' }}">
                                    <label for="cp_password" class="col-md-4 control-label">管理面板密码</label>
                                    <div class="col-md-6">
                                        <input id="cp_password" type="text" class="form-control" name="cp_password"
                                               value="{{ old('cp_password') }}" required>

                                        @if ($errors->has('cp_password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('cp_password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('domain') ? ' has-error' : '' }}">
                                    <label for="domain" class="col-md-4 control-label">免费二级域名</label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input name="domain" id="domain" type="text" class="form-control" placeholder="domain" aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2">.naiba.ga</span>
                                        </div>
                                        @if ($errors->has('domain'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('domain') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-danger">确认注册</button>
                                </div>
                                <hr>
                                <p class="text-right">
                                    &copy;Naiba
                                </p>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
