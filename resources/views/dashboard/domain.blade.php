@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="page-header">
                <h3>我的域名
                    <small>域名列表</small>
                </h3>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>域名</th>
                        <th>到期</th>
                        <th>联系人</th>
                        <th>QQ</th>
                        <th>解析</th>
                        <th>删除</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($domains as $domain)
                        <tr>
                            <th scope="row">{{$domain->id}}</th>
                            <td>{{$domain->name}}</td>
                            <td>{{$domain->expirationdate}}</td>
                            <td>{{$domain->contact_name}}</td>
                            <td>{{$domain->contact_qq}}</td>
                            <td><a href="/dashboard/domain/{{$domain->name}}/dns"><span
                                            class="glyphicon glyphicon-edit"></span></a></td>
                            <td>
                                <form id="deleteDomain{{$domain->id}}" method="POST"
                                      action="/dashboard/domain/{{$domain->name}}">{{csrf_field()}}{{ method_field('DELETE') }}
                                    <span onclick="$('#deleteDomain{{$domain->id}}').submit()"
                                          class="glyphicon glyphicon-remove text-danger"></span></form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <hr>
            <p class="text-right">
                &copy;Naiba
            </p>
        </div>
    </div>
@endsection