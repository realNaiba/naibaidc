@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="page-header">
                <h3>我的主机
                    <small>主机列表</small>
                </h3>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>型号</th>
                        <th>域名</th>
                        <th>开通时间</th>
                        <th>登陆管理面板</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($hostings as $hosting)
                        <tr>
                            <th scope="row">{{$hosting->id}}</th>
                            <th>Free Ⅰ 型</th>
                            <td>{{$hosting->domain}}.naiba.ga</td>
                            <td>{{$hosting->created_at}}</td>
                            <td><a class="btn btn-xs btn-primary" href="#"
                                   onclick="event.preventDefault();document.getElementById('login-cp-form{{$hosting->id}}').submit();">
                                    管理
                                </a>

                                <form id="login-cp-form{{$hosting->id}}" action="http://cpanel.naiba.ga/login.php" method="POST"
                                      style="display: none;" target="_black">
                                    <input name="language" value="Chinese_simplified"/>
                                    <input name="uname" value="{{$hosting->cp_username}}" />
                                    <input name="passwd" value="{{$hosting->cp_password}}" />
                                </form></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <hr>
            <p class="text-right">
                &copy;Naiba
            </p>
        </div>
    </div>
@endsection