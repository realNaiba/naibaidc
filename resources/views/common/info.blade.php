@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-{{$status}}">
                    <div class="panel-heading">提示信息</div>

                    <div class="panel-body">
                        {{$msg}}
                    </div>
                </div>
                <div class="text-center">
                    <a href="{{$link['url']}}" class="btn btn-danger">{{$link['title']}}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
