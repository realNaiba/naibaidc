@extends('layouts.app')

@section('content')
    <div class="container">
        <nb-whois></nb-whois>
        <nb-domain-list></nb-domain-list>
    </div>
@endsection
