@extends('layouts.app')

@section('content')
    <div class="container">
        {{--域名搜索框--}}
        <nb-domain-search></nb-domain-search>
        {{--支持的后缀--}}
        <nb-domain-list></nb-domain-list>
    </div>
@endsection
