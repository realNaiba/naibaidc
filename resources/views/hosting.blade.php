@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="page-header">
                <h1>免费主机
                    <small>Naiba Hosting</small>
                </h1>
            </div>
            <div class="panel panel-warning">
                <div class="panel-body">
                    <p>
                        <b>注意！</b>免费空间使用需遵循以下规则：<br>
                        1. 禁止放置违反所在国家法律的内容。<br>
                        2. 禁止放置大量占用公共资源的应用（ps.秒赞）<br>
                        3. 请加入官方群随时获得最新公告，进行交流技术<br>
                        <b class="text-danger">4. 系统更新频率会很高，随时暂停服务进行升级迁移，还是那句话，加群</b><br>
                        5. 请合理使用免费资源，尊重版权、尊重劳动<br>
                        <b>「官方群」</b> <i class="text-danger">521197317</i><br>
                    </p>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Free Ⅰ 型</th>
                        <th>Free Ⅱ 型</th>
                        <th>Free Ⅲ 型</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">主机类型</th>
                        <td>PHP</td>
                        <td>PHP</td>
                        <td>PHP</td>
                    </tr>
                    <tr>
                        <th scope="row">流量</th>
                        <td>20G</td>
                        <td>无限</td>
                        <td>无限</td>
                    </tr>
                    <tr>
                        <th scope="row">空间</th>
                        <td>100M</td>
                        <td>无限</td>
                        <td>无限</td>
                    </tr>
                    <tr>
                        <th scope="row">可绑域名数</th>
                        <td>3个</td>
                        <td>无限</td>
                        <td>无限</td>
                    </tr>
                    <tr>
                        <th scope="row">数据库个数</th>
                        <td>10个</td>
                        <td>无限</td>
                        <td>无限</td>
                    </tr>
                    <tr>
                        <th scope="row">价格</th>
                        <td>$0.00</td>
                        <td>待定</td>
                        <td>待定</td>
                    </tr>
                    <tr>
                        <th scope="row">技术支持</th>
                        <td><span class="glyphicon glyphicon-remove text-danger"></span></td>
                        <td><span class="glyphicon glyphicon-remove text-danger"></span></td>
                        <td><span class="glyphicon glyphicon-ok text-success"></span></td>
                    </tr>
                    <tr>
                        <th scope="row"></th>
                        <td><a href="/dashboard/hosting/add" class="btn btn-xs btn-primary">开通</a></td>
                        <td>请加群</td>
                        <td>请加群</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
