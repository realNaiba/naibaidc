<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDnsrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dnsrecords',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('domain_id');
            $table->bigInteger('dnspod_id')->comment('DNSPOD ID');
            $table->string('sub_domain')->comment('主机记录');
            $table->string('record_type')->default('@')->comment('记录类型');
            $table->string('record_line')->comment('记录线路');
            //$table->string('record_line_id')->comment('线路的ID');
            $table->string('value')->comment('记录值');
            //1-20
            $table->string('mx')->nullable()->comment('MX优先级');
            //1-604800
            $table->string('ttl')->comment('TTL');
            //[“enable”, “disable”]
            $table->string('status')->nullable()->comment('记录初始状态，默认为”enable”');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dnsrecords');
    }
}
