<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->comment('用户ID');
            $table->bigInteger('dnspod_id')->comment('Dnspod Domain ID');
            $table->string('name')->comment('域名');
            $table->timestamp('expirationdate')->comment('域名过期时间');

            $table->string('contact_name')->comment('联系人姓名');
            $table->string('contact_qq')->comment('联系人QQ');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}
