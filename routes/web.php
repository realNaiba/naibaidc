<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
/** 网站首页 */
Route::get('/', function () {
    return view('welcome');
});
Route::get('hosting',function(){
    return view('hosting');
});
/** WHOIS */
Route::get('whois', 'HomeController@whois')->name('whois');
/** 管理后台 */
Route::group(['prefix' => '/dashboard', 'middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('dashboard');
    })->name('dashboard');

    /** 管理域名 */
    Route::get('domain', 'Dashboard\DomainController@index')->name('dashboard.domain');
    Route::get('domain/{domain}/reg', 'Dashboard\DomainController@reg');
    Route::post('domain/{domain}', 'Dashboard\DomainController@regHandler');
    Route::delete('domain/{domain}', 'Dashboard\DomainController@delete');

    /** 管理解析 */
    Route::get('domain/{domain}/dns', 'Dashboard\DnsController@index');
    Route::post('domain/{domain}/dns', 'Dashboard\DnsController@add');
    Route::delete('domain/{domain}/dns', 'Dashboard\DnsController@delete');

    /** 管理主机 */
    Route::get('hosting','Dashboard\HostController@index')->name('dashboard.hosting');
    Route::get('hosting/add',function (){
        return view('dashboard.hosting.add');
    });
    Route::post('hosting/add','Dashboard\HostController@add');
});